# Projeto de Construção de um Protótipo de Prótese de Mão

![Video](https://gitlab.com/automacao_feec_unicamp/protese_de_mao/wikis/blob/video.mp4)

Esse projeto consiste na construção de um protótipo de uma prótese de mão para pessoas que sofreram amputação.
A atuação na prótese será feita com o uso de servo motores e a visualização dos resultados será feita em um simulador (V-REP).

As diferentes posições dos dedos da prótese serão definidas pelo usuário através de uma interface que combinará leituras de Eletromiografia (EMG) e de um acelerômetro.
Estes sensores serão instalados de forma que consigam captar contrações dos músculos do braço (EMG) e a correspondente orientação.

Mais informações: [wiki](https://gitlab.com/automacao_feec_unicamp/protese_de_mao/wikis/home)

## Autores

Amadeu do Nascimento Junior, Dandara T. G. Andrade
