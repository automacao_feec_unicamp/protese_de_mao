#include "eHealth.h"
#include "tcp_client.h"
#include "DynamixelSerial1.h"
#include <stdio.h>

#define SIZE_ESTADO 4

int last5[5] = {0,0,0,0,0}; //Armazena a media movel
bool contracao = false; //Verifica se ocorreu contracao
long tempoEntreContracao = 0; //Guarda o tempo entre duas contracoes
timeval timeLast, timeContraction; 

// ---- Estados ---------
enum Estados{
  FC = 1,
  BC,
  DC,
  AtrasadoC,
  EC
};

// ------- Flags -------

bool inAnalog = false; //está no modo analogico
bool inTrigger = false; //waiting for a trigger
bool inPosition = false; //esta em alguma posicao

int estados[SIZE_ESTADO] = { };
int index_estado = 0; //variavel usada para varrer vetor de estados;
tcp_client client;

void sendPositionToHand(int id1, int junta1, int id2, int junta2, int id3, int junta3);
void vibraMotor(int pin, long duracao, int repeticao);
void initializeDynamixel(){
  Dynamixel.begin(9600,2); //inicializa a serial na velocidade 9600 e usa o pino 2 para controlar a direcao
  system("stty -F /dev/ttyAMA0 1000000"); //altera a velocidade da serial para 1Mbs. Pode ser colocado direto no arquivo do Dynamixel.
}

void initialize(){
  gettimeofday(&timeLast,NULL);
  eHealth.initPositionSensor();
  client.conn("10.10.1.120",27010); //should pass this address by arg
  initializeDynamixel();
}

void atualizaMediaMovel(int data){
  int aux2 = 0;
  int aux = last5[4];
  last5[4] = data;
  for (int i=3;i>=0;i--){
    aux2 = last5[i];
    last5[i] = aux;
    aux = aux2;
  }
}

int calculaMediaMovel() {
  int aux2 = 0;
  for (int i=0;i<5;i++){
    aux2 = aux2 + last5[i]; 
  }
  return aux2/5;
}

bool verificaContracao(){
  int EMG = eHealth.getEMG();
  //printf("EMG: %d ",EMG);

  // tempo entre contracoes
  gettimeofday(&timeContraction,NULL);
  tempoEntreContracao = (timeContraction.tv_sec - timeLast.tv_sec)*1000.0; //in ms
  tempoEntreContracao += (timeContraction.tv_usec - timeLast.tv_usec)/1000.0; //in ms

  int media = calculaMediaMovel();
  //printf("Media: %d \n",media);	    
  
  if (EMG > media+15 && tempoEntreContracao > 500) { //contracao detectada
    printf("CONTRACAO\n");
    timeLast = timeContraction;
    return true;
  }
  else {
    atualizaMediaMovel(EMG);
    return false;
  }
}

void resetAll() {
  index_estado = 0;
  for (int i=0;i<SIZE_ESTADO;i++) estados[i] = 0;
  inTrigger = false;
  // vibra o motor mostrando que cancelou
  vibraMotor(4,100,3);
  // mao relaxada
  client.send_data("*\n");
  client.send_data("relaxedhand\n");
  client.send_data("*\n");
  sendPositionToHand(1,10,2,100,3,100);
}

void addEstado(int estado) {
  if (index_estado < SIZE_ESTADO) {
    estados[index_estado] = estado;
    index_estado++;
  }
  else {
    //resetAll();
  }
}

int maquinaEstados(uint8_t posicao, bool contracao) {
   //printf("\n Posicao %d\n",posicao); 
  if (contracao) {
    if (tempoEntreContracao <= 5000) {
      switch (posicao) {
        case 1:
	  printf("FrenteC\n");
	  addEstado(FC);
	  vibraMotor(1,100,3);
	  break;
        case 4:  
        case 5:
        case 6:  
	  printf("BaixaC\n");
	  addEstado(BC);
	  vibraMotor(2,100,3);
	  break;
        case 2:
	  printf("DireitaC\n");
	  addEstado(DC);
	  vibraMotor(3,100,3);
	  break;
        case 3:
	  printf("EsquerdaC\n");
	  resetAll();
	  break;
        default:
	  printf("Posicao nao reconhecida \n");
      }	
    } else {
      printf("Confirmação-C\n");
      vibraMotor(4,500,1);
      addEstado(AtrasadoC);
    }    
  }
}
void verificaEstados() {
  int local_index = index_estado-1;
  if (estados[local_index] == AtrasadoC) {
    if (local_index == 1) {
      if (estados[local_index-1] == FC) {
	//indicador ativo
	client.send_data("*\n");
	client.send_data("fingerPoint\n");
	client.send_data("*\n");
	sendPositionToHand(1,128,2,31,3,300);
	printf("Indicador Ativo\n");
      }
      else if (estados[local_index-1] == BC){
	//abdução
	printf("Abducao\n");
	client.send_data("*\n");
	client.send_data("fingerAbduction\n");
	client.send_data("*\n");
	sendPositionToHand(1,0,2,300,3,300);
      }
      else if (estados[local_index-1] == DC){
	// gancho
	printf("gancho\n");
	/*client.send_data("*\n");
	client.send_data("fingerPoint\n");
	client.send_data("*\n");
	sendPositionToHand(1,0,2,300,3,300);*/
      }
      else {
	resetAll();
      }	  
    }
    else if (local_index == BC) {
      if (estados[local_index-1] == FC) {
	if (estados[local_index-2] == FC) {
	  // Coluna
	  printf("Coluna\n");
	}
	else if (estados[local_index-2] == BC) {
	  // Beslicar
	  if (inTrigger){
	    client.send_data("*\n");
	    client.send_data("trigger\n");
	    client.send_data("*\n");
	    sendPositionToHand(1,128,2,300,3,300);
	    // must implement a time control structure to do the triggering
	    return;
	  }
	  client.send_data("*\n");
	  client.send_data("pinchGrip\n");
	  client.send_data("*\n");
	  sendPositionToHand(1,0,2,300,3,300);
	  inTrigger = true;
	  printf("Beliscar\n");
	}
	else if (estados[local_index-2] == DC) {
	  // Pegar com precisão, com dedos fechados
	  if (inTrigger){
	    client.send_data("*\n");
	    client.send_data("trigger\n");
	    client.send_data("*\n");
	    sendPositionToHand(1,126,2,180,3,290);
	    // must implement a time control structure to do the triggering
	    return;
	  }
	  client.send_data("*\n");
	  client.send_data("precisionCloseGrip\n");
	  client.send_data("*\n");
	  sendPositionToHand(1,126,2,0,3,290);
	  printf("Pegar com precisao\n");
	}	
      }
      else if (estados[local_index-1] == BC){
	if (estados[local_index-2] == FC) {
	  // Pegar mouse
	  if (inTrigger){
	    client.send_data("*\n");
	    client.send_data("trigger\n");
	    client.send_data("*\n");
	    sendPositionToHand(1,92,2,40,3,46);
	    // must implement a time control structure to do the triggering
	    return;
	  }
	  client.send_data("*\n");
	  client.send_data("mouseGrip\n");
	  client.send_data("*\n");
	  sendPositionToHand(1,92,2,0,3,46);
	  printf("Pegar mouse\n");
	}
	else if (estados[local_index-2] == BC) {
	  // Apontar
	  printf("Apontar\n");
	}
	else if (estados[local_index-2] == DC) {
	  // Analógico
	  printf("Analogico\n");
	  // se preocupar, pois este modo possui um comando de parada!!!
	  inTrigger = true;
	  inAnalog = true;
	}
      }
      else if (estados[local_index-1] == DC){
	if (estados[local_index-2] == FC) {
	  // Mão aberta
	  printf("Mao aberta\n");
	  client.send_data("*\n");
	  client.send_data("openPalmGrip\n");
	  client.send_data("*\n");
	  sendPositionToHand(1,27,2,27,3,27);
	}
	else if (estados[local_index-2] == BC) {
	  // Pegar Copo
	  printf("Pegar copo\n");
	}
	else if (estados[local_index-2] == DC) {
	  // Pegar colher
	  printf("Pegar Colher\n");
	}
      }
      else {
	resetAll();
      }
    }
    else if (local_index == DC) {
      if (estados[local_index-1] == BC) {
	if (estados[local_index-2] == BC) {
	  if (estados[local_index-3] ==FC) {
	    // Apertar com 3 dedos
	    printf("Apertar com 3 dedos \n");
	  }	  
	}
      }
      if (estados[local_index-1] == DC){
	if (estados[local_index-2] == DC){
	  if (estados[local_index-3] ==FC){
	    // Pegar com precisão, com dedos abertos
	    if (inTrigger){
	      client.send_data("*\n");
	      client.send_data("trigger\n");
	      client.send_data("*\n");
	      sendPositionToHand(1,126,2,180,3,0);
	      // must implement a time control structure to do the triggering
	    return;
	    }
	    client.send_data("*\n");
	    client.send_data("precisionOpenGrip\n");
	    client.send_data("*\n");
	    sendPositionToHand(1,126,2,0,3,0);
	    printf("Pegar com precisao, dedos abertos?\n");
	    
	  }	  
	}
      }
    }
    else{
      printf("Comando não reconhecido\n");
      return;
      }
    inPosition = true; //protese em alguma posicao de pegada
  }
}

void sendPositionToHand(int id1, int junta1, int id2, int junta2, int id3, int junta3){
  Dynamixel.move(id1,junta1);
  Dynamixel.move(id2,junta2);
  Dynamixel.move(id3,junta3);
}

void readMotorsPosition(int * position){

}

void vibraMotor(int pin, long duracao, int repeticao){
  timeval timeNow, timeAux;
  long tempoEntrePulsos;
  for (int i=0;i<repeticao;i++) {
    gettimeofday(&timeAux,NULL);
    while (tempoEntrePulsos < duracao) {
      gettimeofday(&timeNow,NULL);
      tempoEntrePulsos = (timeNow.tv_sec - timeAux.tv_sec)*1000.0; //in ms
      tempoEntrePulsos += (timeNow.tv_usec - timeAux.tv_usec)/1000.0; //in ms
    }
  }
}

void loop() {
  uint8_t position = eHealth.getBodyPosition();
  //eHealth.printPosition(position);
  //printf("\n");

  contracao = verificaContracao();
  if (contracao && !inPosition && !inTrigger) {
    //maquinaEstados(position,contracao);
    printf("Contração ");
    maquinaEstados(position,contracao);
  }
  else if (contracao && inTrigger) {
    //trigger Mode
    verificaEstados();
  }
  else if(contracao && inPosition && !inTrigger) {
    //return to rest
    }

  verificaEstados();

  //  client.send_data("*\n");
  //  client.send_data("powerGrip\n");
  //client.send_data("*\n");
  //verificaEstados();
}

int main(){
  initialize();
  while(1) loop();
  return 0;
}
